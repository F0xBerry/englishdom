#!/bin/bash
file_list=()
dirs_list=('my_dir_1' 'my_dir_2')

unset duplicates # ensure that array is empty
declare -A duplicates

for i in "${dirs_list[@]}"; do
    for FILE in "$i"/*; do
        fname=`basename $FILE`       
        if [[ -z ${duplicates[$fname]} ]]; then
        file_list+=("$fname")
        fi
        duplicates["$fname"]=1
    done
done
for f in "${file_list[@]}"; do
    echo $f
done