#!/usr/bin/env python3
import os


def main(dlist):
    file_list = []
    for d in dlist:
        for (dirpath, dirnames, filenames) in (os.walk(d)):
            for filename in filenames:
                file_list.append(filename)

    file_list = list(dict.fromkeys(file_list))
    file_list.sort()
    for file in file_list:
        print(file)


if __name__ == '__main__':
    dirs_list = ['my_dir_1', 'my_dir_2']
    main(dirs_list)
